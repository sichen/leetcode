http://bookshadow.com/leetcode/

分类：https://cspiration.com/leetcodeClassification#10301

1.3.1. Array
4_Median_of_Two_Sorted_Arrays   用 归并排序，可能用二分查找更好，  sys.maxint 用法
11.Container_With_Most_Water   1.1.2019 更像是从数学角度而不是从计算机（算法）角度解决问题
16.3Sum_Closest                1.1.2019 和11的算法很类似，移动左右两个指针相向移动，直到指针交换左右
18.4Sum                        1.4.2019
26.Remove_Duplicates_from_Sorted_Array  11.24.2018
27. Remove Element
26. Remove Duplicates from Sorted Array 11.9.2018    替换 和遍历
41.First_Missing_Positive 11.12.2018

46.Permutations  1.4.2019  迭代
47.Permutations_II  1.4.2019 没太懂
48.Rotate_Image      1.5.2019   zip 命令 旋转矩阵
49.Group_Anagrams    1.5.2019   字典的键必须是不可修改的，所以list不行，tuple 元组行
54.Spiral_Matrix    12.24.2018     zip 命令 旋转矩阵！！！！
56.Merge_Intervals   12.24.2018  sorted 命令key和cmp
59.Spiral_Matrix_II  12.26.2018  
74.Search_a_2D_Matrix                         忘了
75.Sort_Colors   11.18.2018

80.Remove_Duplicates_from_Sorted_Array_II  11.9.2018
81.Search_in_Rotated_Sorted_ArrayII   忘了
88.Merge_Sorted_Array   11.18.2018   考察 sort
90.Subsets_II          12.27.2018    用好 res+=这个命令，特别是当res为矩阵的时候
95.Unique_Binary_Search_Trees_II   1.7.2019   用了三个for  [x,y,z for x in xx for y in yy for z in zz]
283.Move_Zeroes   11.18.2018
134.Gas_Station     12.09.2018

164.Maximum_Gap  11.20.2018  不难

169.Majority_Element     12.02.2018
189.Rotate_Array    12.23.2018     用list[:] 就可以保证是在本地替换的，而不是产生一个新的list 而且一定要用正数index
122.Best_Time_to_Buy_and_Sell_Stock_II    12.10.2018  
118.Pascal's_Triangle    12.11.2018 
119.Pascal's_Triangle_II   12.12.2018 
217.Contains_Duplicate     12.13.2018
219.Contains_Duplicate_II   12.13.2018
228.Summary_Ranges     1.10.2019      ###很好的小技巧，ranges[-1][1:] = n,
230.Kth_Smallest_Element_in_a_BST   1.10.2019  yield 生成器 很好得看看 ！！！！！！！！！
238.Product_of_Array_Except_Self  12.17.2018  两个方向各进行一遍
240.Search_a_2D_Matrix_II    1.11.2019   命令 any
209.Minimum_Size_Subarray_Sum     12.19.2018  要想用二分查找，必须要对矩阵修改，变成之前所有元素的求和，这样才能是排序过的序列，才能用二分查找
215.Kth_Largest_Element_in_an_Array  12.19.2018
268.Missing_Number                 1.19.2019
274.H-Index                        1.19.2019
275.H-Index_II                     1.20.2019
287.Find_the_Duplicate_Number      1.19.2019
290.Word_Pattern      1.18.2019   find 和index 和map的用法！！
350.Intersection_of_Two_Arrays_II        1.23.2019  collections.Counter 要很熟

1.3.2. String
3. longest substring without repeating characters: 滑动窗口法, 如何用字典存储字符串中每个字符的个数，如何更新起始点和最大长度。
6. ZigZag Conversion: operator, reduce, join 等函数的用法,和字符串连接的操作。
14 Longest Common Prefix    

17.Letter_Combinations_of_a_Phone_Number   1.1.2019
22.Generate_Parentheses                    1.4.2019 难 调用自己
28. Implement strStr

49. Anagrams_copy    哈希表，sorted, filter map reduce 函数，collections 模块
58. Length of Last Word    rstrip()方法 rindex 方法

125.Valid_Palindrome        11.21.2018   isalnum() 方法
131.Palindrome_Partitioning   1.16.2019  一行
151.Reverse_Words_in_a_String     11.21.2018  split 方法  split后 string变成了 list
165.Compare_Version_Numbers  12.20.2018  没什么难度，只是有很多version number定义的特例

168.Excel_Sheet_Column_Title  12.01.2018   因为余数1为A 余数0为Z，到Z时不是正好加65，所以要num-1再求余求模！！
171.Excel_Sheet_Column_Number  12.02.2018
187.Repeated_DNA_Sequences    12.23.2018   也是用默认字典的方法  
179.Largest_Number            1.16.2019    sort 方法 里面的cmp 的option
205. Isomorphic Strings     12.13.2018
241.Different_Ways_to_Add_Parentheses   1.11.2019 分治策略
242.Valid_Anagram        12.17.2018 
299.Bulls_and_Cows    1.20.2019       用map和operator 好理解 还有就是如何格式化
306.Additive_Number 1.21.2019    itertools 用的好，处理leading zero处理的好b != str(int(b)) startswith方法
392.IsSubsequence  1.30.2019  迭代器 iter命令
443.String_Compression  11.19.2018





1.3.3. Math
1. two sum :字典+enumerate 命令（同时列出数据下标，和数据）  remove,set 排除重复元素，
15.3Sum    12.28.2018 第三个数存入字典
29.Divide_Two_Integers   1.2.2019  用左移的方法做除法  
31.Next_Permutation      1.2.2019
50.Pow     1.5.2019  按位与
60.Permutation_Sequence   12.25.2018  
66. Plus One copy:  建立flag
69. sqrt    牛顿迭代法  newton's methods 
7. Reverse Integer
43.Multiply_Strings  11.9.2018 
202.Happy Number    11.19.2018  是不是快乐数都会循环下去而不是无限不循环
166.Fraction_to_Recurring_Decimal  12.20.2018   divmod 命令，同时给出商和余数
167.two_sum_II     12.01.2018   用 list做搜索还是太慢了 就是 判断 x in list 这种命令，用字典还是快很多
172.Factorial_Trailing_Zeroes  12.02.2018   除以5 就能看出有几个5 。
204.Count_Primes     12.12.2018  用filter 慢了，用prime[i*i:n:i] = [0]*len(prime[i*i:n:i])
223.Rectangle_Area   1.7.2019  困了
231.Power_of_Two     12.14.2018
258.Add_Digits       1.18.2019  数学的理解很牛

292.Nim_Game         1.21.2019 
343.Integer_Break    3.3.2019
367.Valid_Perfect_Square   1.23.2019  牛顿法


 
1.3.4. Tree 

## Binary Tree 和Binary search Tree 不是一个东西！！！！！！ 二叉树就是一个节点最多有两个子节点，二叉搜索树在二叉树的基础上又有：每个节点都不比它左子树的任意元素小，而且不比它的右子树的任意元素大。

94. Binary Tree Inorder Traversal(9.3.2018) 二叉树中序遍历：知识点：中序遍历：root在中间，但这里的二叉树不像书中说的左孩<父节点<右孩；  pop（）方法。
95.Unique_Binary_Search_Trees_II  1.12.2019 应该也是分治策略 建立一个左右节点都有的tree是个很好的想法
98*.Validate_Binary_Search_Tree    1.12.2019  父亲不要和孩子比，而是到孩子的时候反过来和父亲比，还有是孙子和爷爷比
100*. same tree(9.3.2018): 判断两个二叉树是否相同，递归。

102.Binary_Tree_Level_Order_Traversal   12.04.2018  同时定义了先序遍历和层序遍历，先序遍历中加入了新的自变量level
103.Binary_Tree_Zigzag_Level_Order_Traversal   1.13.2019  用两个for就可以表示一行节点的所有子节点。用[::1]，[::-1]控制方向
105*.Construct_Binary_Tree_from_Preorder_and_Inorder_Traversal   1.14.2019  recursive 
106*.Construct_Binary_Tree_from_Inorder_and_Postorder_Traversal   1.14.2019
107.Binary_Tree_Level_Order_Traversal_II   12.10.2018
108*.Convert_Sorted_Array_to_Binary_Search_Tree  12.10.2018   和105的思路差不多
109*.Convert_Sorted_List_to_Binary_Search_Tree                    1.14.2019    while 里面用三个变量就可以得到左半部分的结尾的节点
110*.Balanced_Binary_Tree         12.11.2018    先编程测量高度，然后再比较高度，都是用递归 #仅仅高度不差还不行，子树也必须是balanced的
111*.Minimum_Depth_of_Binary_Tree   12.04.2018  有点类似于 dynamic programming
112*.copy_Path_Sum 11.12.2018 递归法
173.Binary_Search_Tree_Iterator     1.17.2019 思路很难，很好，generator的产生，yield的用法，找到最大节点，
222.Count_Complete_Tree_Nodes     1.9.2019 用递归 pow函数说明看清楚了树的节点数的本质
226. Invert Binary Tree  12.07.2018
236.Lowest_Common_Ancestor_of_a_Binary_Tree  1.16.2019

1.3.5. Backtracking
31. next permutation: 下一个排     列的定义；如何不占用新内存的交换；如何颠倒list顺序。
46. Pernutation_copy  做递归，python中 函数做递归的表达式
78.Subsets  11.10.2018    用dfs做的！！！  具体在Google drive/leetcode文件夹中
77.combinations  11.11.2018 
39.copy_Combination_Sum 11.12.2018 但是是南郭的思路
117.PopulatingNextRightPointersinEachNodeII     1.29.2019
216.Combination_Sum_III   12.19.2018   别人的思路
357.Count_Numbers_with_Unique_Digits     1.25.2019  标准模板即可



1.3.6. Dynamic Programming 
#####心得：  DP中，如果是和字符串有关，有可能就是2维的思路；如果跟一个数字有关，很可能就要从头一直算到这个数。DP 还有一个典型的特点是 等号左右同时有若干项赋值。比如309的hold, notHold, notHold_cooldown = max(hold, notHold - p), max(notHold, notHold_cooldown), hold + p


5.Longest_Palindromic_Substring  12.28.2018  比较以某个字符串为中心的对称的字符串中哪个最长。
53.Maximum_Subarray 11.19.2018  
62. unique path_copy  动态规划问题，但不知道为什么对中间过程得到的matrix与实际情况不一致。
63. Unique Paths II_9.10.2018
64.Minimum_Path_Sum  12.25.2018
70. Climbing stairs  : 其实也是动态规划问题。
################### 复习DP 2.27.2019  
91.Decode_Ways                12.27.2018
96.Unique_Binary_Search_Trees    变形的dp问题
120.Triangle   1.15.2019  一行解决  
121.Best_Time_to_Buy_and_Sell_Stock     11.21.2018 
139.wordBreak   11.11.2018    二维 
152.Maximum_Product_Subarray  12.19.2018 
################### 复习DP 2.28.2019  
198.House_Robber  1.16.2019   a,b=c,d 等号右边的数同时向等号左边赋值
213.House_Robber_II     2.8.2019 
221.Maximal_Square                    12.18.2018
264.Ugly_Number_II   1.11.2019 注意这种一个list里面2 3个for的用法！！！
279.Perfect_Squares 11.13.2018 
300.Longest_Increasing_Subsequence    12.07.2018   好像还可以用二分查找，看在线疯狂！！！
################### 复习DP 3.2.2019 
303.Range_Sum_Query-Immutable    1.20.2019 还是面向对象的class的结构掌握的不好,因为sumRange 要经常调用，所以不能用sum命令来做
304.Range_Sum_Query_2D-Immutable    1.21.2019
309.Best_Time_to_Buy_and_Sell_Stock _with_Cooldown    1.22.2019   等号后面有三个
322.Coin_Change 11.13.2018
################### 复习DP 3.3.2019


416.Partition_Equal_Subset_Sum   11.19.2018  
474.Ones_and_Zeroes       11.20.2018 
377.Combination_Sum_IV    11.20.2018,2.8.2019 也可以dfs做


368.Largest_Divisible_Subset   2.6.2019 从2算到n

376.WiggleSubsequence     1.30.2019  贪心算法


1.3.7. LinkedList
2. add two numbers: 链表的概念和加法进位操作
19.Remove_Nth_Node_From_End_of_List  1.4.2019 
24._Swap_Nodes_in_Pairs  11.12.2018
47.Insertion_Sort_List     12.24.2018   用dummy node

61.Rotate_List   12.25.2018  先把linked list的头尾相接，再找到新的头把头尾分开
92.1 先做一个翻转链的操作，熟悉下基本概念和思路。https://foofish.net/linklist-reverse.html
92.Reverse_Linked_List_II 12.28.2018 
138.Copy List with Random Pointer 既有链表，又有哈希表，还看懂答案。下次接着看？？？？？？？？？？？？？？？？？？？？？？？？
82.Remove_Duplicates_from_Sorted_List_II   12.25.2018
83.Remove_Duplicates_from_Sorted_List  11.24.2018
86.Partition_List                      12.27.2018
100.Same_Tree  11.24.2018
141. Linked List Cycle 11.29.2018  元组（） tuple比list快
160.Intersection_of_Two_Linked_Lists  12.12.2018
203.Remove_Linked_List_Elements   12.12.2018 
206.Reverse_Linked_List    12.13.2018 
234.Palindrome_Linked_List  12.14.2018
237.Delete_Node_in_a_Linked_List   12.16.2018    就是修改val和next


1.3.8. Binary Search
35.Search Isert Position （9.6.2018）二分查找
33. Search in Rotated Sorted Array （9.6.2018）
162.Find_Peak_Element  11.11.2018
153.Find_Minimum_in_Rotated_Sorted_Array　11.12.2018
278.First_Bad_Version   11.17.2018  
81.Search_in_Rotated_Sorted_ArrayII  11.18.2018
148.Sort_List           12.09.2018 用归并排序排链表，很好
235.Lowest_Common_Ancestor_of_a_Binary_Search_Tree   12.16.2018  忘了 binary search 有左大右小的特点了。
374.Guess_Number_Higher_or_Lower    1.23.2019  注意替换的时候一个是mid+1，一个是mid， 
378.Kth_Smallest_Element_in_a_Sorted_Matrix    1.23.2019  bisect 模块  heapq 模块

1.3.9. Matrix
54. Spiral Matrix （9.7.2018）   # 求余  用百分号 %
36.Valid_Sudoku         12.04.2018 
74.Search_a_2D_Matrix    12.05.2018





1.3.10. DFS & BFS

101*. Symmetric tree（9.4.2018） BFS深度优先搜索，+collections.deque 实现popleft 
104*.Maximum_Depth_of_Binary_Tree  11.25.2018
##################### 复习 3.5.2019 还有点tree的内容
113*.Path_Sum_II             1.14.2019 DFS
114*. Flatten Binary Tree to Linked List（9.3.2018）二叉树 先序遍历。 
##################### 复习 3.9.2019   还有点dfs模版的内容
116*.PopulatingNextRightPointersinEachNode      3.14.2019 
117*.PopulatingNextRightPointersinEachNodeII   1.29.2019  BFS 感觉和116一样
129*.Sum_Root_to_Leaf_Numbers   1.15.2019 因为是走的最深，所以说DFS 前一位的数乘以10再加下一位
130*.Surrounded_Regions      1.13.2019  BFS 之所以叫做 BFS是因为他把与满足条件的某个节点相邻节的所有节点都加到queue中了！
##################### 复习 3.14.2019 
199×.Binary_Tree_Right_Side_View   12.20.2018   主要在于判断已有结果的长度和当前节点的深度的关系
200×.NumberOfLand                  12.23.2018 和130一样，是BFS
210×.Course_Schedule_II           1.13.2018 用 collections.defaultdict(set) 不用判断key是否存在
257×.Binary_Tree_Paths     1.17.2019    dfs 标准答案，不要看第一个那个取巧的答案



39&40.Combination_Sum    1.4.2019   将dp，dfs和bfs的帖子 https://www.jianshu.com/p/a46ee4ed2a8f

78.Subsets  11.10.2018    用dfs做的！！！  具体在Google drive/leetcode文件夹中
77.combinations  11.11.2018  12.25.2018 方法2很好！！！
79.Word_Search     12.07.2018
93.Restore_IP_Addresses     1.7.2019 DFS


127.Word_Ladder             1.15.2019 BFS


310.Minimum_Height_Trees         1.22.2018 BFS



1.3.11. Stack & PriorityQueue
71. Simplify Path (9.7.2018) 栈的用法，
150. Evaluate Reverse Polish Notation
155. Min Stack （9.7.2018）
84.Largest_Rectangle_in_Histogram  11.14.2018
155. Min Stack    11.30.2018  关于最小栈的方法很好，小于等于当前的值才推进栈
232.Implement_Queue_using_Stacks  12.14.2018 把list的-1位置当做front


1.3.12. Bit Manipulation
89. Gray Code_copy  （9.8.2018） 格雷码，递归方法，<< 移位操作，^ 位异或，相同为0，不同为1； reversed 函数
136. Single Number_copy (9.8.2018） 也是异或操作
137.Single_Number_II     2.6.2019 异或
190_Reverse_Bits
136.Single_Number  11.25.2018   异或 相同取0 与0亦或取本身   reduce 命令
191.Number_of_1_Bits  12.03.2018
201.Bitwise_AND_of_Numbers_Range  12.17.2018    左边相同，异或操作
260.Single_Number_III              1.18.2019  原码反码补码 异或去重 https://www.zhihu.com/question/38206659 位运算有什么奇技淫巧？！！！
318.Maximum_Product_of_Word_Lengths   2.6.2019
338.Counting_Bits                2.6.2019
342.Power_of_Four               2.6.2019
349.IntersectionofTwoArrays    1.30.2019  按位与 集合


1.3.13. Topological Sort
207×.Course_Schedule          12.18.2018      入度等概念   210 比207 思路更清晰，结构也更整齐。
332.ReconstructItinerary     1.30.2019 

1.3.14. Random


1.3.15. Graph
332.ReconstructItinerary   1.30.2019  eulerian path



1.3.16. Union FInd


1.3.17. Trie
208.Implement_Trie   12.18.2018  字典树 新学的一种数据结构
211.AddandSearchWord_Data_structure_design       1.27.2019  

1.3.18. Design


1.3.19 SQL

175. Combine_Two_Tables  12.03.2018  数据库题目，学习基本命令



1.3.20 Hash Table



算法导论：
Algorithms_insertion_sort  10.1.2018 插入排序
Algorithms_merge_sort  10.1.2018     归并排序   sys.maxint 用的好
Algorithms_quick_sort  10.2.2018     快速排序
Algorithms_dp_01knapsack  11.20.2018 01背包问题
Algorithms_topological_sorting         12.18.2018 拓扑排序
Algorithm_bit_manipulation         2.1.2019 


interview:
interview_1_str_reverse:10.21.2018  字符串翻转
Interview_Reverse_and_Add_Function    11.20.2018   翻转相加后看是不是回文















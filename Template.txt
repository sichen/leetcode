Backtracking:  leetcode 77题
模板：
https://blog.csdn.net/fightforyourdream/article/details/12866861
例题：
class Solution(object):
    def combine(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """
        def dfs(start, valuelist):       #要把dfs函数放在combine的最前面！！
            if self.count == k: 
                ret.append(valuelist)
                return
            for i in range(start, n + 1):
                self.count += 1
                dfs(i + 1, valuelist + [i])
                self.count -= 1
        ret = []
        self.count = 0    #这里之所以设置一个self.count 是因为count是全局变量
        dfs(1, [])
        return ret


DFS: leetcode 78题

模板：
https://blog.csdn.net/renwotao2009/article/details/52993277
例题：
class Solution:    #leetcode 78 
    # @param S, a list of integer
    # @return a list of lists of integer
    
    def subsets2(self, S):
        def dfs(depth, start, valuelist):
            res.append(valuelist)
            if depth == len(S): return
            for i in range(start, len(S)):
                dfs(depth+1, i+1, valuelist+[S[i]])
        S.sort()
        res = []
        dfs(0, 0, [])
        return res



BFS: http://www.cnblogs.com/HectorInsanE/archive/2010/11/09/1872656.html
leetcode 130 210 都差不多，感觉BFS的模板不是很明显











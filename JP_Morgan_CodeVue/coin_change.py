import sys


while True:
    line=sys.stdin.readline().strip() 
    if not line:  
        break
    a=map(int,line.split())
    coin=[1,3,5]
    dp=[float('inf')]*(a[0]+1)
    dp[0]=0
    for i in range(a[0]+1):
        for j in coin:
            if j<=i:
                dp[i]=min(dp[i],dp[i-j]+1)

    print dp[-1] if dp[-1]<=a else -1

'''
try:
    while True:
        line=sys.stdin.readline().strip() 
        if not line:  
            break
        a=map(int,line.split())
        coin=[1,3,5]
        dp=[float('inf')]*(a[0]+1)
        dp[0]=0
        for i in range(a[0]+1):
            for j in coin:
                if j<=i:
                    dp[i]=min(dp[i],dp[i-j]+1)

        print dp[-1] if dp[-1]<=a else -1
except:
    pass
'''









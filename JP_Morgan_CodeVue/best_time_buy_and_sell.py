import sys


while True:
    line=sys.stdin.readline().strip() 
    if not line:  
        break
    a=map(int,line.split())      
    minimum=a[1]
    dp=[0]*(a[0]+1)
    for i in range(2,a[0]+1):
        dp[i]=max(dp[i-1],a[i]-minimum)
        minimum=min(minimum,a[i])
    print dp[-1]











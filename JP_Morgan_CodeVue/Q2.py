


import sys

for line in sys.stdin:
    #line=sys.stdin.readline().strip() 

    a=map(int,line.split())      
    minimum=a[1]
    dp=[0]*(a[0]+1)
    for i in range(2,a[0]+1):
        dp[i]=max(dp[i-1],a[i]-minimum)
        minimum=min(minimum,a[i])
    print int(dp[-1])
import sys


while True:
    line=sys.stdin.readline().strip() 
    if not line:  
        break
    a=map(int,line.split(','))      
    minimum=maximum=a[0]
    dp=[0]*len(a)
    for i in range(1,len(a)):
        dp[i]=max(dp[i-1],abs(a[i]-minimum),abs(maximum-a[i]))
        minimum=min(minimum,a[i])
        maximum=max(maximum,a[i])
    print dp[-1]








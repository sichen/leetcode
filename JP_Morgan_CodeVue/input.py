# -*- coding: utf-8 -*
# https://blog.csdn.net/qq_35203425/article/details/82693774
# https://blog.csdn.net/tan197/article/details/82735472

import sys

try:
    while True:
        line=sys.stdin.readline().strip() #输入是一个字符串
        if not line:  #再按一个回车 说明不是一行了，就break出去
            break
        a=line.split() #中间用空格的情况 生成一个list，每个元素是字符串的一个字符
        a=line.split(',') #中间用逗号的情况，如何把他们分开
        #print line   # 'a b c d 4'
        print a          #['a', 'b', 'c', 'd', '4']   和这个等效#sys.stdout.write(str(a)+'\n')
        #     sys.stdout.write(" ".join(str(x) for x in a[:p1]))   #见 string compress.py

except:
    pass 

'''
考试中的格式是

import sys
for line in sys.stdin:
    

    print line
   
''


'''
>>> w=sys.stdin.readline().strip()
a b c d 4
>>> w
'a b c d 4'
>>> w.split()
['a', 'b', 'c', 'd', '4']
>>> aaa=sys.stdin.readline()   # 会打印出换行符
a b d
>>> aaa
'a b d\n'
>>> aaa=sys.stdin.readline().strip()
a b d d  
>>> aaa
'a b d d'

sys.stdout.write('hello' + '\n')
print('hello')
上面两行是等价的


'''